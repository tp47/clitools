#!/usr/bin/env python3

import os
import sys
import re


def filename_normalizer(files):
    for old_file in files:
        if "/" in old_file:
            path, _filename = old_file.rsplit("/", 1)
            path += "/"
        else:
            path = ""
            _filename = old_file

        _filename = _filename.lower()

        if "." in _filename:
            filename, ext = _filename.rsplit(".", 1)
            ext = "." + ext
        else:
            filename = _filename
            ext = ""

        new_filename = re.sub("[^a-z0-9_]", "_", filename)
        new_file = f"{path}{new_filename}{ext}"

        counter = 0
        original_filename = new_filename
        while os.path.exists(new_file):
            counter += 1
            new_filename = f"{original_filename}_{counter}"
            new_file = f"{path}{new_filename}{ext}"

        try:
            os.rename(old_file, new_file)
        except FileNotFoundError:
            print(f"File {old_file} does not exist.")


if __name__ == "__main__":
    files = sys.argv[1:]
    filename_normalizer(files)
