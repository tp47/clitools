#!/usr/bin/env python3
import os
from pathlib import Path


def file_group_merge(proj_path):
    """
    Take a list of files and merge them to directories
    based on the same extensions. Name the directory
    the same as the extension.
    """
    files = [str(f) for f in proj_path.listdir()]
    for old_path in files:
        currdir, filename = old_path.rsplit("/", 1)

        if "." in filename:
            _, ext = filename.rsplit(".", 1)
            new_dir = Path(currdir) / ext
        else:
            new_dir = Path(currdir)

        if not new_dir.exists():
            os.mkdir(new_dir)

        new_path = new_dir / filename
        os.rename(old_path, new_path)


if __name__ == "__main__":
    file_group_merge()
