import pytest
from clitools.bin.normalize import filename_normalizer


@pytest.mark.parametrize("test_input,expected",
                         [
                             ("Uppercase.PY", "uppercase.py"),
                             ("@#$Something", "___something"),
                             ("lowerUpperlowerUpper",
                              "lowerupperlowerupper"),
                         ])
def test_single_filename(tmpdir, test_input, expected):
    p = tmpdir / test_input
    p.write("")

    files = [str(f) for f in tmpdir.listdir()]
    filename_normalizer(files)

    _, new_filename = str(tmpdir.listdir()[0]).rsplit("/", 1)
    assert new_filename == expected


@pytest.mark.parametrize("test_inputs,expected_filenames",
                         [
                             (["Uppercase.PY", "upperCase.py"],

                              ["uppercase.py", "uppercase_1.py"]),

                             (["Uppercase$%.PY", "upperCase.py",
                               "UpperCase.py"],
                              ["uppercase__.py", "uppercase.py",
                               "uppercase_1.py"]),
                         ])
def test_filename_conflict(tmpdir, test_inputs, expected_filenames):
    for tmp_file in test_inputs:
        p = tmpdir / tmp_file
        p.write("")

    files = [str(f) for f in tmpdir.listdir()]
    filename_normalizer(files)

    new_filenames = [str(f).rsplit("/", 1)[1] for f in tmpdir.listdir()]
    assert sorted(expected_filenames) == sorted(new_filenames)
