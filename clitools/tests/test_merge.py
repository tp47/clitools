import os
from os import path
import pytest
from clitools.bin.merge import file_group_merge


test_cases = [
    (["main.py", "main.c"],
     ["py/main.py", "c/main.c"]),

    (["main.py", "sys.py", "mega.c", "super.c"],
     ["c/mega.c", "c/super.c",
      "py/sys.py", "py/main.py"]),

    (["noext"], ["./noext"]),

    (["noext", "main.py"],
     ["./noext", "py/main.py"]),
]


@pytest.mark.parametrize("test_inputs,expected_paths", test_cases)
def test_simple_merge(tmpdir, test_inputs, expected_paths):
    """
    Create temp files for testing
    Run the merge module over the test cases temp files
    List every file from recursive walking through dirs/sub_dirs
    Check against new file paths with expected_paths
    """
    for f in test_inputs:
        _f = tmpdir / f
        _f.write("")

    file_group_merge(proj_path=tmpdir)

    dir_walk = list(os.walk(tmpdir))
    _, sub_dirs, _ = dir_walk[0]

    new_file_paths = []
    for dir_step in dir_walk:
        fullpath, dirs, files = dir_step
        relpath = path.relpath(fullpath, tmpdir)
        for f in files:
            new_file_paths.append(relpath + "/" + f)

    assert sorted(expected_paths) == sorted(new_file_paths)
